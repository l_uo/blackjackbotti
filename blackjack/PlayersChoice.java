package blackjack;

import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

/** PlayersChoice-luokalla tarkoitetaan pelin osaa, jossa aloitusjako on jo tehty ja siirrytään vaiheeseen,
*	jossa pelaajat voivat valita ottavatko he lisää kortteja vai eivät.
*	PlayersChoice-luokan alustukseen tarvitaan kaikki kolme moottoria, server-olio sekä painallusnappien oliot 
*	pelaajien valintojen seuraukseen, sekä pelaajien lukumäärä. PlayersChoice käynnistetään kutsumalla sen action-metodia mainin kautta.
*	PlayersChoice-olion action-metodi kommunikoi tietokoneen java-ohjelman kanssa serverin metodeja käyttäen, aivan kuten aloitusjaossa.
*	Action-metodin loputtua myös ohjelma loppuu.
*	@author Eelis, Lauri, Petteri
 */

public class PlayersChoice  {
	public MasiinaServer server;
	private RegulatedMotor eka;
	private RegulatedMotor toka;
	private RegulatedMotor rotate;
	private int korttiluettu;
	private int jaot = 0;
	private double kaantyminen = 50;
	private ButtonOne one;
	private ButtonTwo two;
	private Aloitusjako aloitusjako;
	private int lukumaara;
	
	/**
	 * 
	 * @param eka Moottori joka asettaa kortin luettavaksi.
	 * @param toka Moottori joka lähettää kortin pelaajalle.
	 * @param rotate Moottori joka hoitaa EV3-robotin kääntymisen.
	 * @param server Oliolla saadaan yhteys tietokoneeseen.
	 * @param aloitusjako Ei tarvita.
	 * @param one Nappi jota painetaan kun halutaan kortteja.
	 * @param two Nappi jota painetaan kun ei haluta kortteja.
	 * @param lukumaara Integer-muutuja pelaajien lukumäärästä. 
	 */
	public PlayersChoice(RegulatedMotor eka, RegulatedMotor toka, RegulatedMotor rotate, 
			MasiinaServer server, Aloitusjako aloitusjako, ButtonOne one, ButtonTwo two, int lukumaara) 
	{
		this.eka = eka;
		this.toka = toka;
		this.rotate = rotate;
		this.server = server;
		this.one = one;
		this.two = two;
		this.aloitusjako = aloitusjako;
		this.lukumaara = lukumaara;
	}

	/** Action-metodi on PlayersChoice-luokan päätoiminto.
	*	Se kommunikoi tietokoneen SecondDeal-luokan metodin playersActions kanssa, odottaen käskyjä kyseiseltä
	*	metodilta. Metodissa odotetaan pelaajien tekemiä päätöksiä, kunnes jokaisen pelaajan vuoro loppuu.
	*	Seuraavaksi robotti jakaa kortit itselleen, ja ohjelma loppuu. 
	*/
	public void action() {
		
		korttiluettu = 0;
		int kaantymiset = 0;
		eka.setSpeed(400);
		toka.setSpeed(1000);
		rotate.setSpeed(100);
		//Ilmoitetaan, että EV3 valmiina.
		server.sendToComputer(1);
		server.readFromComputer();
		
		while (kaantymiset < lukumaara) {
			System.out.println("olen loopissa");
			
			if (one.returnButtonPressed()) {
				//annetaan kortti
				eka.rotate(-260);
				Delay.msDelay(500);
				eka.rotate(100);
				Delay.msDelay(1000);
				
				server.sendToComputer(1);
				korttiluettu = server.readFromComputer();
				//odotetaan että kortti on luettu
				toka.rotate(360);
				Delay.msDelay(500);
				
				if (korttiluettu == 2) {
					rotate.rotate((int) kaantyminen);
					kaantymiset++;
				}

			} else if (two.returnButtonPressed()) {
				//käännetään bottia
				server.sendToComputer(2);
				rotate.rotate((int) kaantyminen);
				kaantymiset++;
			}
		}
		//jakajan vuoro
		//tarkastetaan onko jakajalla yli 17
		rotate.rotate((int) (-kaantyminen * (kaantymiset / 2.0)));
		korttiluettu = server.readFromComputer();
		
		//kortteja luetaan niin kauan kunnes koneelta tulee 3
		while (korttiluettu != 3) {
			eka.rotate(-260);
			Delay.msDelay(500);
			eka.rotate(100);
			server.sendToComputer(1);
			korttiluettu = server.readFromComputer();
			if (korttiluettu == 1) {
				toka.setSpeed(200);
				toka.rotate(100);
				Delay.msDelay(1000);
			}
		}
		toka.rotate(100);
		rotate.rotate((int) (-kaantyminen * (kaantymiset / 2.0)));
		System.out.println("Pena sammutetaan:");
	}
}
