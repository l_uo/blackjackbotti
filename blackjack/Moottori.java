package blackjack;

import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;

/** Moottori-luokassa alustetaan EV3 liitetyt kolme RegulatedMotor-luokkaa, 
*	joiden kautta EV3 kontrolloi portteihin liitettyjä moottoreita.
*	Yla-moottorilla tarkoitetaan moottoria, joka asettaa kortin luettavaksi alustalle.
*	Kaannos-moottori kääntää robottia sen jakaessa kortteja.
*	Ala-moottori lähettää kortit pelaajille.
*	@author Eelis, Lauri, Petteri
 */

public class Moottori {
	RegulatedMotor yla;
	RegulatedMotor kaannos;
	RegulatedMotor ala;
	
	/**
	 * Moottori-konstruktorissa alustettaan kolme liitettyä EV3-moottoria.
	 */
	public Moottori() {
		try
		{
			this.yla = new EV3LargeRegulatedMotor(MotorPort.D);
			this.kaannos = new EV3LargeRegulatedMotor(MotorPort.B);
			this.ala = new EV3MediumRegulatedMotor(MotorPort.C);
		}
		catch (Exception e) 
		{
			System.out.println("Moottorit ei kiinni.");
			System.exit(0);
		}
	}
	
	/**
	 * 
	 * @return yla EV3LargeRegulatedMotor-olio, joka asettaa kortin alustalle josta kortti luetaan.
	 */
	public RegulatedMotor getYla() {
		return yla;
	}
	/**
	 * 
	 * @return ala EV3MediumRegulatedMotor-olio, joka lähettää kortin pelaajalle. 
	 */
	public RegulatedMotor getAla() {
		return ala;
	}
	
	/**
	 * 
	 * @return kaannos EV3LargeRegulatedMotor-olio, joka suorittaa EV3-robotin kääntymisen.
	 */
	public RegulatedMotor getKaannos() {
		return kaannos;
	}
}

