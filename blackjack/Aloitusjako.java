package blackjack;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

/** Luokka robotin aloitusjaolle. Luokkaolion luontiin tarvitaan EV3-moottorien oliot,
*	EV3 yhteydessä oleva serveri ja pelaajien määrä.
*	Aloitusjako aloitetaan kutsumalla action-metodia Main.java tiedoston kautta.
*	Action-metodin on tarkoitus kommunikoida tietokoneeseen liitetyn java-ohjelman kanssa
*	käyttäen serverin luku- ja lähetyskäskyjä.
*	@author Eelis, Lauri, Petteri  
*/

public class Aloitusjako  {
	public MasiinaServer server;
	private RegulatedMotor eka;
	private RegulatedMotor toka;
	private RegulatedMotor rotate;
	private int pelaajamaara;
	private int korttiluettu;
	private int jaot = 0;
	private double kaantyminen = 50;
	public volatile boolean dealstatus = false;
	
	
	/**
	 * 
	 * @param eka Moottori joka asettaa kortin luettavaksi.
	 * @param toka Moottori joka lähettää kortin pelaajalle.
	 * @param rotate Moottori joka hoitaa EV3-robotin kääntymisen.
	 * @param server Oliolla saadaan yhteys tietokoneeseen.
	 * @param pelaajamaara Integer-muutuja pelaajien lukumäärästä.
	 */
	public Aloitusjako(RegulatedMotor eka, RegulatedMotor toka, RegulatedMotor rotate, 
			MasiinaServer server, int pelaajamaara) 
	{
		this.eka = eka;
		this.toka = toka;
		this.rotate = rotate;
		this.server = server;
		this.pelaajamaara = pelaajamaara; 
	}
	
	/** Aloitusjako-luokan päätoiminto. Action-metodi jää odottamaan, että tietokoneen 
	*	AloitusJakoTietokone-luokan ekaJako-metodia ollaan suorittamassa. Tämän jälkeen
	*	molemmat metodit kommunikoivat keskenään ja robotti jakaa ensimmäiset kortit.
	*/
	public void action() {
		
		//moottorien nopeudet
		
		eka.setSpeed(400);
		toka.setSpeed(1000);
		rotate.setSpeed(100);
		
		//otetaan vastaan käsky että peli voi alkaa 
		server.readFromComputer();
		
		while (jaot < (pelaajamaara * 2 + 1) ) {
			//ampuu kortin lukemiseen
			eka.rotate(-260);
			Delay.msDelay(500);
			eka.rotate(100);
			Delay.msDelay(1000);
			//lähettää datan tietokoneelle että kortti voidaan lukea
			server.sendToComputer(1);
			korttiluettu = server.readFromComputer();

			System.out.println("kortti luettu");

			//jokaiselle pelaajalle jaetaan yksi kortti
			if (korttiluettu == 1 && jaot < pelaajamaara) {
				//ampuu kortin pelaajalle ja kääntyy seuraavan pelaajan puoleen
				toka.rotate(360);
				Delay.msDelay(500);
				rotate.rotate((int) kaantyminen);
			}
			//dealerin vuoro ottaa yksi kortti
			else if (korttiluettu == 1 && jaot == pelaajamaara) {
				rotate.rotate((int) (-kaantyminen * (pelaajamaara/2.0)));
				toka.setSpeed(200);
				toka.rotate(100);
				toka.setSpeed(1000);
				Delay.msDelay(1000);
				rotate.rotate((int) (-kaantyminen * (pelaajamaara/2.0)));
			}
			//jaetaan jokaiselle pelaajalle viimeiset kortit
			else if (korttiluettu == 1 && jaot > pelaajamaara) {
				//ampuu kortin pelaajalle ja kääntyy seuraavan pelaajan puoleen
				toka.rotate(360);
				Delay.msDelay(500);
				rotate.rotate((int) kaantyminen);
			}
			jaot++;	
			korttiluettu = 0;
		}
		//palautetaan robotti dealerin korttien paikalle
		rotate.rotate((int) (-kaantyminen * (pelaajamaara/2.0)));
		eka.rotate(-260);
		Delay.msDelay(500);
		eka.rotate(100);
		server.sendToComputer(1);
		server.readFromComputer();
		toka.setSpeed(200);
		toka.rotate(100);
		
		rotate.rotate((int) (-kaantyminen * (pelaajamaara/2.0)));
		
		
	}
}
