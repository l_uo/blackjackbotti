package blackjack;

import java.util.ArrayList;

public class Pelaaja {
	private static int lukumaara = 0;
	private int id;
	private ArrayList<Integer> kortit = new ArrayList();
	private int sum = 0;
	
	public Pelaaja() {
		this.id = lukumaara;
		lukumaara++;
	}
	
	public void lisaaKortti(int kortti) {
		kortit.add(kortti);
		sum += kortti;
		System.out.println(sum);
		
		for (int i = 0; i < kortit.size(); i++) {
			if (sum > 21 && kortit.get(i) == 11) {
				kortit.set(i, 1);
				sum -= 10;
			}
		}
	}
	
	public static void main(String[] args) {
		Pelaaja pelaaja1 = new Pelaaja();
		Pelaaja pelaaja2 = new Pelaaja();
		
		System.out.println(pelaaja1.id + " " + pelaaja2.id);
		
		pelaaja1.lisaaKortti(3);
		pelaaja1.lisaaKortti(11);
		pelaaja1.lisaaKortti(8);
		pelaaja1.lisaaKortti(10);
		pelaaja2.lisaaKortti(6);
		pelaaja2.lisaaKortti(10);
		pelaaja2.lisaaKortti(11);
		pelaaja2.lisaaKortti(11);
		pelaaja2.lisaaKortti(9);
		
		System.out.println("pelaaja1 kortit: " + pelaaja1.sum);
		System.out.println("pelaaja2 kortit: " + pelaaja2.sum);
	}
}

