package blackjack;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import lejos.hardware.Button;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

public class Main {
	public static void main(String[] args) {
		int pelaajamaara = 1;
		Moottori moottori = new Moottori();
		RegulatedMotor yla = moottori.getYla();
		RegulatedMotor ala = moottori.getAla();
		RegulatedMotor kaannos =  moottori.getKaannos();
		MasiinaServer server = new MasiinaServer();
		Aloitusjako aloitusjako = new Aloitusjako(yla, ala, kaannos, server, pelaajamaara);
		ButtonOne buttonOne = new ButtonOne();
		ButtonTwo buttonTwo = new ButtonTwo();
		PlayersChoice choice = new PlayersChoice(yla, ala, kaannos, server, aloitusjako, buttonOne, buttonTwo, pelaajamaara);
		
		buttonOne.setDaemon(true);
		buttonOne.start();
		buttonTwo.setDaemon(true);
		buttonTwo.start();
		
		aloitusjako.action();
		choice.action();
		System.exit(0);
	}
}
