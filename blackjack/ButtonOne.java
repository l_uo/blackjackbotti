package blackjack;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * ButtonOne on demoni, joka on päällä kokoajan ohjelmaaa suorittaessa.
 * Luokasta tehty olio tarkkailee robottiin kytketyn ensimmäisen napin painalluksia.
 * Luokan olion luonnissa tehdään heti yhteys EV3-porttiin, luodaan porttiin kiinnitetystä
 * napista sensori, jonka painalluksia mitataan run()-metodin avulla.
 * Jos nappia on painettu, ButtonOne-olio muistaa sen buttonPressed-muuttujan avulla.
 * Vasta kun returnButtonPressed-metodi on kutsuttu, voidaan havaita uusi painallus.
 * @author Eelis, Lauri, Petteri
 *
 */

public class ButtonOne extends Thread {
	Port port;
	SensorModes sensor;
	SampleProvider touch;
	float[] sample;
	private volatile boolean buttonPressed = false;
	
	/**
	 * Konstruktorissa alustetaan kosketus-anturi.
	 * Luodaan yhtes EV3-porttiin ja luodaan porttiin kiinnitetystä napista sensori.
	 */
	public ButtonOne() {
		try {
			port = LocalEV3.get().getPort("S1");
			sensor = new EV3TouchSensor(port);
			touch = ((EV3TouchSensor)sensor).getTouchMode();
			sample = new float[touch.sampleSize()];
		} catch (Exception e) {
			System.out.println("Button luonti epäonnistui");
			System.exit(0);
		}
	}
	
	/** run-metodi on Thread-yläluokasta peritty metodi, joka on kokoajan toiminassa
	*	sen omassa säikeessään. Run-metodi mittaa napin painalluksia, ja säilyttää yhden painalluksen
	*	muistiin luokan buttonPressed-muuttujaan.
	*/
	public void run() 
	{
		while (true) {
			
			touch.fetchSample(sample, 0);
			
			if (sample[0] > 0.5) {
				buttonPressed = true;
				Delay.msDelay(1000);
			}
		}
	}
	
	/** returnButtonPressed-metodin tarkoitus on antaa muille ohjelman toiminnoille tieto siitä, että
	*	nappia on painettu. Jos nappia on painettu, se on tallennettu buttonPressed-muuttujaan. Tämä metodi
	*	"nollaa" painalluksen, jonka jälkeen uusi painallus voidaan tallentaa.
	*/	
	public boolean returnButtonPressed() 
	{
		if (buttonPressed == true) {
			System.out.println("Button 1 painettu");
			buttonPressed = false;
			return true;
	} else {
		return false;
	}
	}
}
