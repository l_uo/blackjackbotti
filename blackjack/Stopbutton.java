package blackjack;

import lejos.hardware.Button;
import lejos.hardware.ev3.EV3;
import lejos.robotics.subsumption.Behavior;

/** Stopbutton-luokka toimii robotin hätäpysäytyksenä.
*	Stopbutton-olio tarkkailee EV3:n oikeaa nappia, ja jos sitä painetaan ohjelma pysähtyy.
*	@author Eelis, Lauri, Petteri
*/

public class Stopbutton implements Behavior{
	private volatile boolean suppressed = false;
	
	/**
	 * 
	 * @return true Kun nappia painetaan.
	 * @return false Kun nappia ei paineta.
	 */
	public boolean takeControl() { 
		if (Button.RIGHT.isDown()) {
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * 
	 */
	public void suppress() {
		suppressed = true;
	}
	
	/**
	 * action-metodi suorittaa ohelman sammuttamisen kun StopButton-nappia painetaan.
	 */
	public void action() {
		System.out.println("moi");
		suppressed = false;
		System.exit(0);
	}
}
