package blackjack;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.utility.Delay;
import lejos.hardware.motor.EV3MediumRegulatedMotor;

/**
 * Testauksessa käytetty luokka.
 * @author Eelis
 *
 */
public class Masiina {
public static void main(String[] args) {
	RegulatedMotor ekajako = new EV3LargeRegulatedMotor(MotorPort.D);
	RegulatedMotor tokajako = new EV3MediumRegulatedMotor(MotorPort.C);
	RegulatedMotor rotate = new EV3LargeRegulatedMotor(MotorPort.B);
	
	ServerSocket serv;
	Socket s = null;
	DataInputStream in = null;
	DataOutputStream out = null;
	int luettu;
	
	//ekajako.rotate() täytyy olla minus alkuinen luku, jotta pyörii oikeaan suuntaan
	
	//nopeudet
	ekajako.setSpeed(400);
	tokajako.setSpeed(1000);
	
	
	ekajako.rotate(-260);
	Delay.msDelay(500);
	ekajako.rotate(100);
	Delay.msDelay(1000);
	
	
	//kortin luku
	try {
		serv = new ServerSocket(1111);
		s= serv.accept();
		out = new DataOutputStream(s.getOutputStream());
		in = new DataInputStream(s.getInputStream());
		for (int i = 1; i < 14; i++) {
			out.writeInt(1);
			out.flush();
			
			
			System.out.println("moi pena taal");
			luettu = in.readInt();
			
			if (luettu == 1) {
				tokajako.rotate(360);
				Delay.msDelay(500);
				ekajako.rotate(-260);
				Delay.msDelay(500);
				ekajako.rotate(100);
				Delay.msDelay(1000);
				
				
			}
			
			luettu = 0;
		}
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		in.close();
		out.close();
		s.close();
	} catch (IOException e ) {
		e.printStackTrace();
	}
	System.exit(0);
	
	}
	
	
}
