package blackjack;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/** MasiinaServer-luokalla robotti luo konstruktorissa server-socketin, johon tietokone ohjelma yhdistää.
*	Samalla luodaan Input ja OutputStreamit, joiden kautta tietokoneen kanssa tapahtuva kommunikointi tapahtuu.
*	Tietokoneen kanssa tapahtuvaan kommunikointiin käytetään luokan metodeja sendToComputer (lähettää dataa tietokoneelle)
*	ja readFromComputer (lukee dataa tietokoneelta). Molemmissa metodeissa kommunikointiin käytetään int-arvoja.
*	@author Eelis, Lauri, Petteri
 */

public class MasiinaServer {
	ServerSocket serv;
	Socket s = null;
	DataInputStream in = null;
	DataOutputStream out = null;
	
	/**
	 * Konstruktorissa luodaan ServerSocketti johon tietokone yhdistää,
	 * DataInput- ja DataOutputStreamit joiden avulla tietokoneen kanssa voidaan kommunikoida.
	 */
	public MasiinaServer() {
		try {
			this.serv = new ServerSocket(1111);
			this.s= serv.accept();
			this.in = new DataInputStream(s.getInputStream());
			this.out = new DataOutputStream(s.getOutputStream());
			
		} catch (IOException e) {
			System.out.println("server luonti epäonnistui.");
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return in Palauttaa DataInputStream-olion.
	 */
	public DataInputStream getIn() {
		return in;
	}
	
	/**
	 * 
	 * @return out Palauttaa DataOutputStream-olion.
	 */
	public DataOutputStream getOut() {
		return out;
	}
	
	/**
	 * 
	 * @return serv Palauttaa ServerSocket-olion.
	 */
	public ServerSocket getServ() {
		return serv;
	}
	
	/**
	 * 
	 * @return s Palauttaa Socket-olion.
	 */
	public Socket getS() {
		return s;
	}
	
	/** close-metodilla voidaan sulkea kaikki EV3:seen luodut yhteydet.
	*	server-socket suljetaan, aivan kuten DataInput- ja DataOutput -streamit.
	*	Metodia tulisi kutsua, kun robotti on tehnyt tehtävänsä ja ohjelma lopetetaan.
	*/
	public void close() {
		try {
			in.close();
			out.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** sendToComputer-metodia käytetään robotin ja tietokoneen välisessä kommunikoinnissa.
	*	Metodi kirjoittaa muuttujan DataOutputStreamiin, josta tietokoneen on luettava se omalla 
	*	SocketTietokone-luokan metodilla readFromEv3.
	*	@Param i Integer-muuttuja joka lähetetään EV3:sta tietokoneelle.
	 */
	public void sendToComputer(int i) {
		try {
			out.writeInt(i);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** readFromComputer-metodilla luetaan tietokoneelta tuleva muuttuja DataInputStream-olion avulla.
	*	Tämä luettu integer-muuttuja palautetaan, jotta muut ohjelman muut luokat voivat käyttää tietoa hyväkseen. 	
	*/
	public int readFromComputer() {
		int i = 0;
		try {
			i = in.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return i;
	}
}
