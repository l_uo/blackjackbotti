<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.control.Button?>
<?import javafx.scene.control.ButtonBar?>
<?import javafx.scene.control.CheckBox?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.control.TextArea?>
<?import javafx.scene.control.TextField?>
<?import javafx.scene.image.ImageView?>
<?import javafx.scene.layout.AnchorPane?>
<?import javafx.scene.text.Font?>

<AnchorPane prefHeight="600.0" prefWidth="800.0" xmlns="http://javafx.com/javafx/10.0.1" xmlns:fx="http://javafx.com/fxml/1" fx:controller="view.BlackjackOverviewController">
   <children>
      <Label layoutX="62.0" layoutY="14.0" prefHeight="43.0" prefWidth="294.0" text="BLACKJACKBOT ∞" AnchorPane.leftAnchor="62.0">
         <font>
            <Font name="Book Antiqua Bold" size="29.0" />
         </font>
      </Label>
      <ButtonBar layoutX="68.0" layoutY="57.0" prefHeight="40.0" prefWidth="231.0" />
      <TextArea layoutX="5.0" layoutY="97.0" prefHeight="490.0" prefWidth="390.0" scrollTop="10.0" text="RULES:&#10;&#10;1. Blackjack may be played with one to eight decks of 52-card decks.&#10;2. Aces may be counted as 1 or 11 points, 2 to 9 according to pip value, and tens and face cards count as ten points.&#10;3. The value of a hand is the sum of the point values of the individual cards. Except, a &quot;blackjack&quot; is the highest hand, consisting of an ace and any 10-point card, and it outranks all other 21-point hands.&#10;4. After the players have bet, the dealer will give two cards to each player and two cards to himself. One of the dealer cards is dealt face up. The facedown card is called the &quot;hole card.&quot;&#10;5. If the dealer has an ace showing, he will offer a side bet called &quot;insurance.&quot; This side wager pays 2 to 1 if the dealer's hole card is any 10-point card. Insurance wagers are optional and may not exceed half the original wager.&#10;6. If the dealer has a ten or an ace showing (after offering insurance with an ace showing), then he will peek at his facedown card to see if he has a blackjack. If he does, then he will turn it over immediately.&#10;7. If the dealer does have a blackjack, then all wagers (except insurance) will lose, unless the player also has a blackjack, which will result in a push. The dealer will resolve insurance wagers at this time.&#10;8. Play begins with the player to the dealer's left. The following are the choices available to the player: &#10;&#10;a. Stand: Player stands pat with his cards.&#10;b. Hit: Player draws another card (and more if he wishes). If this card causes the player's total points to exceed 21 (known as &quot;breaking&quot; or &quot;busting&quot;) then he loses.&#10;c. Double: Player doubles his bet and gets one, and only one, more card.&#10;d. Split: If the player has a pair, or any two 10-point cards, then he may double his bet and separate his cards into two individual hands. The dealer will automatically give each card a second card. Then, the player may hit, stand, or double normally. However, when splitting aces, each ace gets only one card. Sometimes doubling after splitting is not allowed. If the player gets a ten and ace after splitting, then it counts as 21 points, not a blackjack. Usually the player may keep re-splitting up to a total of four hands. Sometimes re-splitting aces is not allowed.&#10;e. Surrender: The player forfeits half his wager, keeping the other half, and does not play out his hand. This option is only available on the initial two cards, and depending on casino rules, sometimes it is not allowed at all.&#10;&#10;9. After each player has had his turn, the dealer will turn over his hole card. If the dealer has 16 or less, then he will draw another card. A special situation is when the dealer has an ace and any number of cards totaling six points (known as a &quot;soft 17&quot;). At some tables, the dealer will also hit a soft 17.&#10;10. If the dealer goes over 21 points, then any player who didn't already bust will win.&#10;11. If the dealer does not bust, then the higher point total between the player and dealer will win." wrapText="true" AnchorPane.leftAnchor="5.0" />
      <ImageView fitHeight="150.0" fitWidth="161.0" layoutX="494.0" layoutY="238.0" pickOnBounds="true" preserveRatio="true" AnchorPane.rightAnchor="145.0" />
      <Label layoutX="521.0" layoutY="406.0" text="Card number">
         <font>
            <Font size="18.0" />
         </font>
      </Label>
      <Label layoutX="560.0" layoutY="443.0" text="##">
         <font>
            <Font size="24.0" />
         </font>
      </Label>
      <Button layoutX="535.0" layoutY="188.0" mnemonicParsing="false" onAction="#handleStart" text="Start Game" />
      <TextField fx:id="numberField" layoutX="499.0" layoutY="142.0" />
      <Label layoutX="408.0" layoutY="97.0" text="Enter number of players (MAX 4)">
         <font>
            <Font size="23.0" />
         </font>
      </Label>
      <CheckBox layoutX="425.0" layoutY="192.0" mnemonicParsing="false" text="Demomode" />
   </children>
</AnchorPane>
