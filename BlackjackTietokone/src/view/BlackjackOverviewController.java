package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class BlackjackOverviewController {
	

	@FXML
	private TextField numberField;
	
	private Stage dialogStage;
	private boolean startClicked = false;
	private int playerNumber;

	@FXML
	 private void initialize() {
	 }
	 
	 public boolean isStartClicked() {
		 return startClicked;
	 }

	public void setDialogStage(Stage dialogStage) {
		 this.dialogStage = dialogStage;
	 }

	 private boolean isInputValid() {
		 String errorMessage = "";
		 if (numberField.getText() == null || numberField.getText().length() == 0) {
	            errorMessage += "No valid number!\n"; 
	        } else {
	            try {
	                Integer.parseInt(numberField.getText());
	            } catch (NumberFormatException e) {
	                errorMessage += "No valid number (must be an integer)!\n"; 
	            }
	        }
		 if (errorMessage.length() == 0) {
	            return true;
		 }else {
	            // Show the error message.
	            Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(dialogStage);
	            alert.setTitle("Invalid Fields");
	            alert.setHeaderText("Please correct invalid fields");
	            alert.setContentText(errorMessage);
	            
	            alert.showAndWait();
	            
	            return false;
		 }
	 }
	 @FXML
	 private void handleStart() {
		 if (isInputValid()) {
			 playerNumber = Integer.parseInt(numberField.getText());
		 }
	        
	       
	 }
	
}
