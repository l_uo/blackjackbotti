import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import org.opencv.imgproc.Imgproc;

import com.sun.javafx.scene.EnteredExitedHandler;

import javafx.scene.control.Button;
import model.AloitusJakoTietokone;
import model.Player;
import model.SecondDealComputer;
import model.SocketTietokone;

public class ComputerMain {
	public static void main(String[] args) {
		Player player = new Player();
 		SocketTietokone s = new SocketTietokone();
		AloitusJakoTietokone ekajakotietokone = new AloitusJakoTietokone(s, 1);
		Player[] players = ekajakotietokone.ekaJako();
		SecondDealComputer dealsecond = new SecondDealComputer(s, players);
		dealsecond.PlayersActions();
		System.exit(0);
	}
}
