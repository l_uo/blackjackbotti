package model;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.opencv.imgproc.Imgproc;

/**
 * Testaus luokka jossa korteista otetaan kuva 14 kertaa.
 * Ei ole oleellinen, vain testaamisessa käytetty.
 * @author Eelis
 *
 */
public class KorttienConfig {
	public static void main(String[] args) {
		//luodaan socket
				Socket s = null;
				Matching matching = new Matching();
				try {
					s = new Socket("10.0.1.1", 1111);
				} catch (UnknownHostException e) {
							
					e.printStackTrace();
							
				} catch (IOException e) {
					e.printStackTrace();
				}
						
				try {
					for (int i = 1; i < 14; i++) {
						DataInputStream input = new DataInputStream(s.getInputStream());
						DataOutputStream output = new DataOutputStream(s.getOutputStream());
						int on = input.readInt();
						if ( on == 1 ) {
							
							System.loadLibrary("opencv_java2411");
							WriteImage image = new WriteImage();
							image.captureSnapShot();
							image.saveImageConf("redking");
							matching.run("C:\\Users\\melto\\Pictures\\kortitpohja.png" 
									,"C:\\Users\\melto\\Pictures\\Kortit\\redking.png"
									,"C:\\Users\\melto\\Pictures\\templatematch1.png", Imgproc.TM_SQDIFF);
							on = 0;
							output.writeInt(1);
							output.flush();
							TimeUnit.SECONDS.sleep(1);
					}
				}
					
				} catch (IOException e) {
					e.printStackTrace();
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
}
