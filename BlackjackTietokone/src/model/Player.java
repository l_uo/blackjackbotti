package model;


import java.util.ArrayList;

/**
 * Player-Luokka jonka avulla voidaan luoda Player-olioita. 
 *  
 * @author Eelis, Lauri, Petteri
 *
 */
public class Player {
	private static int lukumaara = 0;
	private int id;
	private ArrayList<Integer> cards = new ArrayList();
	private int sum = 0;
	
	/**
	 * Kun Player-olio luodaan jokainen pelaaja saa oman id-arvon jonka avulla
	 * saadaan tietyn pelaajan tiedot.
	 */
	public Player() {
		this.id = lukumaara;
		lukumaara++;
	}
	
	/**
	 * addCard-metodi lisää pelaajan kortteihin uuden kortin ja laskee pelaajan korttien summan.
	 * Jos pelaajalla on korttina ässä ja pelaajan kortien summa menee yli kaksikymmentäyksi,
	 * ässän arvo muuttuu yhdestätoista ykköseksi.
	 * @param kortti Player-oliolle jaettu kortti.
	 */
	public void addCard(int kortti) {
		cards.add(kortti);
		sum += kortti;
		for (int i = 0; i < cards.size(); i++) {
			if (sum > 21 && cards.get(i) == 11) {
				cards.set(i, 1);
				sum -= 10;
			}
		}
	}
	/**
	 * showCards-metodi tulostaa pelaajan kortit terminalissa. Metodia oli tarkoitus käyttää käyttöliittymässä
	 * jotta nähdään mitkä kortit jokaisella pelaajalla on.
	 * @return str  Player-olion korttien arvot string muodossa.
	 */
	public String showCards() {
		String str = "";
		for (int i = 0; i < cards.size(); i++) {
			str = str + cards.get(i) + ", ";
		}
		return str;
	}
	/**
	 * getId-metodilla saadaan tietyn pelaajan id.
	 * @return id Palauttaa Player-olion id:n.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * checkCard-metodille annettaan parametrina Matching-luokasta saadun metodin run() palauttama x-koordinaatti.
	 * Metodissa vertaa x-koordinaattin if-lause ehtoihin ja saa tämän kautta kortin arvon.
	 * @param x Kuvantunnistuksesta saadun kohdan koordinaatti.
	 */
	public void checkCard(double x) {
		System.out.println(x +": x koordinaatti");
		int card = 0;
		double cards_length = 97;
		int position_x = 0;
		
		if (x < 97) {
			card = 2;
		}
		else if (x < 294) {
			card = 3;
		}
		else if (x < 491) {
			card = 4;
		}
		else if (x < 688) {
			card = 5;
		}
		else if (x < 885) {
			card = 6;
		}
		else if (x < 1082) {
			card = 7;
		}
		else if (x < 1279) {
			card = 8;
		}
		else if (x < 1476) {
			card = 9;
		}
		else if (x < 2264) {
			card = 10;
		}
		else if (x >= 2264) {
			card = 11;
		}
		System.out.println(card +" :kortin numero");
		this.addCard(card);
	}
	
	/**
	 * Metodi palauttaa Player-olion korttien summan.
	 * @return sum Player-olion korttien summa yhteensä.
	 */
	public int getSum() {
		return sum;
	}
	
	/**
	 * Testausta varten luotu Main, 
	 * jolla nähdään toimiiko peli-korttien arvojen tallennus pelaajan tietoihin.
	 * @param args
	 */
	public static void main(String[] args) {
		Player pelaaja1 = new Player();
		Player pelaaja2 = new Player();
		
		System.out.println(pelaaja1.id + " " + pelaaja2.id);
		
		pelaaja1.addCard(3);
		pelaaja1.addCard(11);
		pelaaja1.addCard(8);
		pelaaja1.addCard(10);
		pelaaja2.addCard(6);
		pelaaja2.addCard(10);
		pelaaja2.addCard(11);
		pelaaja2.addCard(11);
		pelaaja2.addCard(9);
		
		System.out.println("pelaaja1 kortit: " + pelaaja1.sum);
		System.out.println("pelaaja2 kortit: " + pelaaja2.sum);
	}
}

