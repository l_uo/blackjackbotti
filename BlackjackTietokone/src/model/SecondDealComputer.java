package model;
import java.util.Timer;
import java.util.concurrent.Delayed;

import org.opencv.imgproc.Imgproc;

/**
 * SecondDealComputer-luokka hoitaa pelin osan tietokone puolen, mihin päästään kun pelin ensinmäinen vaihe eli aloitusjako on jo suoritettu.
 * Tässä toisessa pelin vaiheessa pelaajat voivat halutessaan ottaa lisää kortteja tai olla ottamatta. Lopuksi kun jokainen pelaaja on toiminut
 * jakaja eli EV3-robotti ottaa vielä kortteja. Pelin toinen vaihe loppuu jakajan vuoroon.
 * 
 * @author Eelis, Lauri, Petteri
 *
 */
public class SecondDealComputer {
	private SocketTietokone s;
	private Player[] players;
	private Matching matching = new Matching();
	private WriteImage image = new WriteImage();
	
	/**
	 * Konstruktori saa parametreja kaksi kappaletta.
	 * @param s  SocketTietokone-olio jonka avulla voidaan lähettää dataa EV3-robotille.
	 * @param players Player-olioista koostuva lista jonka avulla voidaan päivittää pelaajien tietoja.
	 */
	public SecondDealComputer(SocketTietokone s, Player[] players) {
		this.s = s;
		this.players = players;
	}
	
	/**
	 * PlayersActions-metodissa suoritettaan pelaajien vuorot toimia halutulla kahdella eri tavalla. Metodi oloittaa toiminnan ensinmäisen pelaajan vuorosta.
	 * While-loopin sisällä ollaan niin kauan kunnes pelaajan korttien summa on alle 21. Jos pelaaja saa ensinmäisellä
	 * jaolla 21 tietokone lähettää robotille tiedon siirtyä seuraavalle pelaajalle. Jos pelaajan korttien summa on alle 21 ja 
	 * pelaaja ottaa kortin ja uuden korttin lisättyä pelaajan kortteihin korttien summa on yli  21 while-loop loppuu ja siirrytään seuraavalle pelaajalle. 
	 * Kun jokaisen pelaajan vuoro on käyty läpi tietokone kertoo lopuksi pitääkö jakajan eli EV3-robotin ottaa lisää kortteja.
	 * Robotti ottaa kortteja niin kauan kunnes EV3-robotin korttien summa on yli tai yhtä suuri kuin 17.
	 */
	public void PlayersActions() {
		//(info about s.sendToEV3) if 1 ev3 deals card if 2 next player, 3 means stopping ev3 while loop
		
		//is ev3 ready
		s.readFromEV3();
		System.out.println("Second action to ev3:");
		s.sendToEV3(1);
		int readcard;
		double x;
		
		for (int i = 0; i < players.length - 1; i++) {
			System.out.println("ollaan forin sisällä computer");
		
			//double x where mathing returns coordinate
			while (players[i].getSum() < 21) {
				System.out.println("ollaan while loopin sisällä computer");
				
				readcard = s.readFromEV3();
				System.out.println("luettiin readcard:"+readcard);
				//if player pressed take card 
				if (readcard == 1) {
					image.captureSnapShot();
					image.saveImageConf("dealkortti");
					
					//matching returns x coordinate
					x = matching.run("C:\\Users\\melto\\Pictures\\korttipohja2.png" 
							,"C:\\Users\\melto\\Pictures\\KortitTesti\\dealkortti.png"
							,"C:\\Users\\melto\\Pictures\\KortitTesti\\dealtemplatematch"+i+".png", Imgproc.TM_SQDIFF);
					players[i].checkCard(x);
					System.out.println(players[i].showCards());
					
					//send to ev3 that computer read card
					if (players[i].getSum() < 21) {
						s.sendToEV3(1);
					}
					else {
						s.sendToEV3(2);
					}
				}
				//if player pressed don't take card
				else if (readcard == 2) {
					break;
				}
 			}
			System.out.println("Send ev3 to move to next player.");
		}
		//dealer takes last cards.. and ev3 moves to dealers position
		int i = 0;
		if (players[players.length - 1].getSum() >= 17) {
			s.sendToEV3(3);
		}
		else {
			s.sendToEV3(1);
		}
		while (players[players.length - 1].getSum() < 17) {
			s.readFromEV3();
			image.captureSnapShot();
			image.saveImages(players[players.length - 1].getId(), i);
			
			x = matching.run("C:\\Users\\melto\\Pictures\\korttipohja2.png" 
					,"C:\\Users\\melto\\Pictures\\KortitTesti\\kortti"+players[players.length - 1].getId()+i+".png"
					,"C:\\Users\\melto\\Pictures\\KortitTesti\\templatematch"+players[players.length - 1].getId()+i+".png", Imgproc.TM_SQDIFF);
			i++;
			players[players.length - 1].checkCard(x);
			
			if (players[players.length - 1].getSum() >= 17) {
				s.sendToEV3(3);
			}
			else {
				s.sendToEV3(1);
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
