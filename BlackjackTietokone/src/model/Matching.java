package model;


import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 * Matching-luokka joka sisältää yhden metodin.
 * Tätä luokkaa hyödynnetään jakovuorossa olevan pelaajan pelikortin tunnistamiseen.
 * Kuvan tunnistamiseen on käytetty template-matching systeemiä.
 *
 */
class Matching {
	
	/**
	 * Metodi suorittaa kuvan tunnistamisen etsimällä templateFile kuvaa inFile kuvasta.
	 * @param inFile  Kuva josta halutaan löytää yhtäläisyyksiä.
	 * @param templateFile  Kuva jota estitään inFile kuvasta.
	 * @param outFile  Kuva jossa yhtäläisyyden kohdalle on piirretty neliö.
	 * @param match_method  Kaava jota käytetään yhtäläisyyden löytämiseen.
	 * @return Yhtäläisyyden löydetyn alueen x akselin koordinaatti.
	 */
    public double run(String inFile, String templateFile, String outFile, int match_method) {
        System.out.println("\nRunning Template Matching");

        Mat img = Highgui.imread(inFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        Mat templ = Highgui.imread(templateFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);

        // / Create the result matrix
        int result_cols = img.cols() - templ.cols() + 1;
        int result_rows = img.rows() - templ.rows() + 1;
        Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

        // / Do the Matching and Normalize
        Imgproc.matchTemplate(img, templ, result, match_method);
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

        // / Localizing the best match with minMaxLoc
        MinMaxLocResult mmr = Core.minMaxLoc(result);
        Point matchLoc;
        if (match_method == Imgproc.TM_SQDIFF || match_method == Imgproc.TM_SQDIFF_NORMED) {
            matchLoc = mmr.minLoc;
            System.out.println(matchLoc+":matchLoc from minLoc and mmr: "+ mmr);
        } else {
            matchLoc = mmr.maxLoc;
            System.out.println(matchLoc +":matchLoc from maxLoc and mmr: "+ mmr);
        }

        // / Show me what you got
        Core.rectangle(img, matchLoc, new Point(matchLoc.x + templ.cols(),
                matchLoc.y + templ.rows()), new Scalar(0, 255, 0));

        // Save the visualized detection.
        System.out.println("Writing "+ outFile);
        Highgui.imwrite(outFile, img);
        
        return matchLoc.x;

    }
}

