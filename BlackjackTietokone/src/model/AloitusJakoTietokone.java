package model;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import org.opencv.imgproc.Imgproc;

/**
 * Aloitusjako-luokka, jossa blackjack-korttipelin ensimmäinen vaihe suoritetaan. 
 * Ensimmäisessa vaiheessa jokaiselle pelaajalle ja jakajalle jaetaan kortteja vuorotellen,
 * niin kauan kunnes jokaisella on kaksi korttia. Tämän jälkeen ensimmäinen vaihe loppuu.
 * 
 * @author Eelis, Lauri, Petteri
 *
 */
public class AloitusJakoTietokone {
	private SocketTietokone s;
	private WriteImage image = new WriteImage();
	private int pelaajamaara;
	private Matching matching = new Matching();
	private Player[] pelaajat;
	private double[] x;
	
	/**
	 * Konstruktori jossa alustetaan Player-olioita pelaajamäärän mukaan.
	 * @param s 
	 * @param pelaajamaara
	 */
	public AloitusJakoTietokone(SocketTietokone s, int pelaajamaara) {
		this.s = s;
		this.pelaajamaara = pelaajamaara + 1;
		pelaajat = new Player[this.pelaajamaara];
		
		for (int i = 0; i < this.pelaajamaara; i++) {
			pelaajat[i] = new Player();
		}
	}
	
	/**
	 * ekaJako-metodi jossa tietokone ottaa kuvan kortista aina kun kortti on kameran kohdalla EV3-robotissa.
	 * Metodi-readFromEV3 palauttaa integer-muuttujan yksi kun kortti kameran kohdalla.
	 * Tietokoneen otettua kuvan kortista käytetään matching.run-metodia jolla suoritetaan kuvan tunnistaminen.
	 * Kuvan tunnistamisesta saatu x-koordinaatti annetaan parametrina checkCard-metodille joka päivittää kortin arvon sen pelaajan tietoihin jolle kortti ollaan jakamassa.
	 * Tietokone lähettää tämän jälkeen robotille tiedon sendToEV3-metodilla että kortti voidaan jakaa pelaajalle.
	 * Metodi käy läpi jokaisen pelaajan ja jakajan kortit jonka jälkeen metodi loppuu.
	 * @return pelaajat Player-olioista koostuva lista.
	 */
	public Player[] ekaJako() {
		System.out.println("Annetaan eka käsky");
		s.sendToEV3(1);

		for (int j = 0; j < 2; j++) {
			int i = 0;
			int on = 0;
			int jaot = 0;
			x = new double[this.pelaajamaara + 1];
			while (jaot < pelaajamaara ) {

				on = s.readFromEV3();

				if (on == 1) {
					image.captureSnapShot();
					image.saveImageConf("aloituskortti");
					
					//matching returns x coordinate
					x[i] = matching.run("C:\\Users\\melto\\Pictures\\korttipohja2.png" 
							,"C:\\Users\\melto\\Pictures\\KortitTesti\\aloituskortti.png"
							,"C:\\Users\\melto\\Pictures\\KortitTesti\\templatematch"+j+i+".png", Imgproc.TM_CCORR_NORMED);
					
					System.out.println(i+":i arvo");
					pelaajat[i].checkCard(x[i]);
					
					on = 0;
					jaot++;
					i++;
					s.sendToEV3(1);
				} else {
					System.out.println("Kuvan ottaminen ohitettiin.");
				}
			}
		}
		return pelaajat;
	}
	
}
