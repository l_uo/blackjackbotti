package model;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * SocketTietokone-luokassa luodaan vaadittavat yhteydet tietokoneen puolelle, 
 * jotta EV3-robottiin saadaan yhteys.
 * SocketTietokoneen konstruktorissa luodaan yhteys EV3-robotin server-sockettiin.
 * @author Eelis, Lauri, Petteri
 *
 */
public class SocketTietokone {
	Socket s = null;
	DataOutputStream out = null;
	DataInputStream in = null;
	
	/**
	 * Konstruktorissa luodaan Socketti johon yhdistettään.
	 * DataInput- ja DataOutputStreamit joiden avulla EV3:n kanssa voidaan kommunikoida.
	 */
	public SocketTietokone() {
		Socket s = null;
		try {
			s = new Socket("10.0.1.1", 1111);	
			this.in = new DataInputStream(s.getInputStream());
			this.out = new DataOutputStream(s.getOutputStream());
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @return in Palauttaa DataInputStream-olion.
	 */
	public DataInputStream getIn() {
		return in;
	}
	/**
	 * 
	 * @return out Palauttaa DataOutputStream-olion.
	 */
	public DataOutputStream getOut() {
		return out;
	}
	/**
	 * @return s Palauttaa Socket-olion.
	 */
	public Socket getS() {
		return s;
	}
	
	/**
	 * readFromEV3-metodia käytetään kun halutaan tietokoneen vastaanottaa muuttuja EV3-robotilta.
	 * @return i  Integer-muuttuja joka vastaanotetaan EV3-robotilta.
	 */
	public int readFromEV3() {
		int i = 0;
		try {
			i = in.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return i;
	}
	
	/**
	 * sendToEV3-metodia käytetään muuttujan lähettämiseen EV3-robotille.
	 * Metodia pystytään hyödyntämään kun halutaan robotin siirtyvän seuraavaan vaiheeseen tietokoneen suoritettua tehtävänsä.  
	 * @param i Integer-muuttuja joka lähetetään EV3-robotille.
	 */
	public void sendToEV3(int i) {
		try {
			out.writeInt(i);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Close-metodilla suljetaan kaikki tietokoneen muodostamat yhteydet.
	 * Socket-s , DataInput- ja DataOutput -streamit suljetaan.
	 * Metodia tulisi kutsua kun, tietokone on tehnyt tehtävänsä ja ohjelma lopetetaan.
	 */
	public void close() {
		try {
			in.close();
			out.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
